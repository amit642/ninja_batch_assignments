#!/bin/bash
cd /home/amit/learn
git clone https://github.com/opstree/spring3hibernate
cd spring3hibernate
mvn install
cd target
sudo cp *.war /opt/tomcat/apache-tomcat-9.0.21/webapps
sudo systemctl restart tomcat.service
status_code=$(curl --write-out %{http_code} --silent --output /dev/null http://localhost:8080/Spring3HibernateApp)
if [[ "$status_code" -ne 302 ]] ; then
	echo "Spring3Hibibernate app is sucessfully deploy on tomcat"

else
	echo "Not deploy Spring3Hibernate app"
fi
