#!/bin/bash
if ! dpkg -l nginx | egrep 'îi.*nginx' > /dev/null 2>&1; then
        echo "nginx is installed"
else
        sudo apt update
        sudo apt install nginx
        sudo systemctl start nginx
fi
if ! dpkg -l apache2 | egrep 'îi.*apache2' > /dev/null 2>&1; then
        echo "apache is installed"
else
        sudo apt update
        sudo apt install apache2
fi

