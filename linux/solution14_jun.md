```
Q. How would you check
```
*Memory used by a process(RAM)
 ps -o pid,%mem ax
pmap pid_of_process
*total number of open files by a process
ls /proc/pid_of_process/fd | wc -l
*running duration of a process
ps-o etime –p pid_of_process

```
Q. What is file descriptor
```
In Unix and related computer operating systems, a file descriptor (FD, less frequently fildes) is an abstract indicator (handle) used to access a file or other input/output resource, such as a pipe or network socket. File descriptors form part of the POSIX application programming interface.File descriptor is a simple and rather small integer number indicating an offset into an internal table of open files belonging to your process.  When you call open() or a few other functions, a new file descriptor is created and an entry is made in that internal table.

```
Q. How to kill a process
```
*forcefully
kill -9 pid_of_process
*gracefully
killpid_of_process
kill -15 pid_of_process

```
Q. What are the signals
```
A signal is an asynchronous notification sent to a process or to a specific thread within the same process in order to notify it of an event that occurred
The following table lists out common signals you might encounter and want to use in your programs –

Signal Name| Signal Number |Description
----------------|-------------------|--------------
|SIGHUP |1 |Hang up detected on controlling terminal or death of controlling process|
|SIGINT |2 | Issued if the user sends an interrupt signal (Ctrl + C)|
|SIGQUIT |3 |Issued if the user sends a quit signal (Ctrl + D)|
|SIGFPE |8 |Issued if an illegal mathematical operation is attempted|
|SIGKILL |9 | If a process gets this signal it must quit immediately and will not perform any clean up operations|
|SIGALRM |14 |Alarm clock signal (used for timers)|
|SIGTERM |15 |Software termination signal (sent by kill by default)|

```
Q. What is parent process ID
```
In Linux, an executable stored on disk is called a program, and a program loaded into memory and running is called a process. When a process is started, it is given a unique number called process ID (PID) that identifies that process to the system. If you ever need to kill a process, for example, you can refer to it by its PID. Since each PID is unique, there is no ambiguity or risk of accidentally killing the wrong process (unless you enter the wrong PID).
In addition to a unique process ID, each process is assigned a parent process ID (PPID) that tells which process started it.
The PPID is the PID of the process’s parent.
For example, if process1 with a PID of 101 starts a process named process2, then process2 will be given a unique PID,such as 3240, but it will be given the PPID of 101. It’s a parent-child relationship. A single parent process may spawn several child processes, each with a unique PID but all sharing the same PPID.

```
Q. Print pid of current shell
```
echo$$

```
Q. How to clear a log file of running process
```
“&gt; filename.log”
“true&gt; filename.log”
“cat /dev/null &gt; filename.log”
“cp /dev/null filename.log”
“echo&gt; filename.log”
“truncate –s 0 filename.log”

```
Q. What will happen if you delete a log file of running process
```
If you delete a log file of a running process, then process will not send any log until the process gets restarted. When the process gets restarted, it will create again the log file with the same name.

```
Q. How do you check all the running process in the system.
```
ps –e
ps –A
ps –r
top

```
Q. How do you check those process that are waiting for the resources?
```
ps aux | awk &#39;$8 ~ /D/ { print $0 }&#39;
and  watch -n 1 "(ps aux | awk '\$8 ~ /D/  { print \$0 }')"

```
Q. What init process is responsible for?
```
The program init is the process with process ID 1. It is responsible for initializing the system in the required way. init is started directly by the kernel and resists signal 9, which normally kills processes. All other programs are either started directly by init or by one of its child processes.
The entire process of starting the system and shutting it down is maintained by init. From this point of view, the kernel can be considered a background process whose task is to maintain all other processes and adjust CPU time and hardware access according to requests from other programs.The
alternate and better option as part of init is systemd which is also responsible for starting the system’s other process.

```
Q. What are Running, Waiting, Stopped and Zombie processes
```
Running –It is the current process in the system

Waiting – A process is waiting for an event to occur or for a system resource. Additionally, the kernel also differentiates between two types of waiting processes; 
interruptible waiting processes – can be interrupted by signals
uninterruptible waiting processes – are waiting directly on hardware conditions and cannot be interrupted by any event/signal.

Stopped –A process has been stopped, usually by receiving a signal. For instance, a process that is being debugged.

Zombie – Zombie processes usually occur for child processes, as the parent process still needs to read its child’s exit status. Once this is done using the wait system call, the zombie process is eliminated from the process table. This is known as reaping the zombie process.

```
Q. How do you elevate the priority of a process Setting priority on new processes
```
nice 
Setting Priority on Existing Processes
renice 

```
Q. What are stdin, stdout, and stderr and how do we use them stdin
```


```
Q. How many tables are there in iptables. What filter and nat table responsible for
```
There are four tables in ip tables
1. Filter Table:
Filter is default table for iptables. If you need to use your own table, you need to define it explicitly otherwise you’ll be using filter table.
Iptables’s filter table has the following built-in chains.
INPUT chain: Incoming to firewall. For packets coming to the local server.
OUTPUT chain: Outgoing from firewall. For packets generated locally and going out of the local server.
FORWARD chain: Packet for another NIC on the local server. For packets routed through the local server.
2. NAT Table
Iptable’s NAT table has the following built-in chains.
PREROUTING chain: It alters packets before routing. i.e Packet translation happens immediately after the packet comes to the system (and before routing).
This helps to translate the destination ip address of the packets to something that matches the routing on the local server. This is used for DNAT (destination NAT).
POSTROUTING chain: It alters packets after routing. i.e Packet translation happens when the packets are leaving the system.
This helps to translate the source ip address of the packets to something that might match the routing on the
desintation server. This is used for SNAT (source NAT).
OUTPUT chain: NAT for locally generated packets on the firewall.

3. -Mangle Table
Mangle table is for specialized packet alteration. This alters QOS bits in the TCP header. Mangle table has the following built-in chains.
PREROUTING chain
OUTPUT chain
FORWARD chain
INPUT chain
POSTROUTING chain

4. Raw table
Iptable’sRaw table is for configuration excemptions. Raw table has the following built-in chains.
PREROUTING chain 
OUTPUT chain

```
Q. What are the default policies in iptables and what it signifies.
```
The default chain policy is ACCEPT. Change this to DROP for all INPUT, FORWARD, and OUTPUT chains as shown below.
iptables -P INPUT DROP
iptables -P FORWARD DROP
iptables -P OUTPUT DROP
When you make both INPUT, and OUTPUT chain’s default policy as DROP, for every firewall rule requirement you have, you should define two rules. i.e one for incoming and one for outgoing.

```
Q. What is the difference between -I and -A while applying a rule in iptables.
```
iptables -A appends rules at the end of the ruleset whereas iptables -I inserts the rule at a specific position in the ruleset as you&#39;ve pointed out.
-I, --insert chain [rulenum] rule-specification Insert one or more rules in the selected chain as the given rule number. So, if the rule number is 1, the rule or rules are inserted at the head of the chain. This is also the default if no rule number is specified.

```
Q. What is the kernel module that tracks the connection block ssh and ftp access for vagrant1
machine to vagrant2 machine
```
You can inspect and restrict connections to services based on their connection state. A module within iptables uses a method called connection tracking to store information about incoming connections. You can allow or deny access based on the following connection states: 
    • NEW — A packet requesting a new connection, such as an HTTP request. 
    • ESTABLISHED — A packet that is part of an existing connection. 
    • RELATED — A packet that is requesting a new connection but is part of an existing connection. For example, FTP uses port 21 to establish a connection, but data is transferred on a different port (typically port 20). 
    • INVALID — A packet that is not part of any connections in the connection tracking table. 

```
Q. What is DNAT and SNAT, Explain both with an example
```
## Destination NAT
Destination NAT means, we translate the destination address of a packet to make it go somewhere else instead of where it was originally addressed. For our scenario, it is:
iptables -t nat -A  PREROUTING -d 10.10.10.99/32 -j DNAT –to-destination 192.168.1.101
Now all IP packets coming to our machine’s (A) IP address of 10.10.10.99 will be rewritten and sent to 192.168.1.101 instead. This translation is transparent to the machine the connection is originating from and to machine B.
So if you connect from, say, 172.16.1.10 to 10.10.10.99, the packet will be rewritten upon reaching machine A and be sent to machine B. Machine B will see it coming from 172.16.1.10, it will have no idea that the connection was redirected by 10.10.10.99. This is important because when machine B replies, the FROM address in it’s reply will be it’s own IP address of 192.168.1.101.
This will cause a protocol error on your machine because your machine (172.16.1.10) will be expecting a reply from 10.10.10.99 and instead will receive a reply from 192.168.1.101.
To fix this, we now need to do SNAT – Source Network Address Translation.

## SNAT
We want to do SNAT to translate the from address of our reply packets to make them look like they’re coming from 10.10.10.99 instead of 192.168.1.101. To do this, we need to apply a SNAT iptables rule on a router along the path these reply packets will take. Since our machine A is the default gateway for machine B, we will do the SNAT on machine A as well.
iptables -t nat -A POSTROUTING -s 192.168.1.101/32 -j SNAT –to-source 10.10.10.99
This rewrites our source address to look like the packets are coming from 10.10.10.99 instead of 192.168.1.101.
At this point, try to connect to machine A using SSH and log in using credentials for machine B.
Once you’ve confirmed this works, you can save your iptables configuration with:
service iptables save
You can then configure iptables to run at startup automatically.
/sbin/chkconfig –level 3 iptables on
/sbin/chkconfig –level 5 iptables on
And that concludes a basic DNAT/SNAT configuration. In most situations you may want to narrow down the match criteria to specific protocols or ports. e.g. you can use the “-p tcp –dport 22” flags to match only SSH traffic.




```
Q. Make a shell script that would configure a firewall as below:
- Flush all the rules
- set default DROP policy for INPUT, ACCEPT for FORWARD and OUTPUT chain of filter table
- Allow ssh from vagrant machine1 only
- Allow port 80,443 from everywhere
- Allow ping from outside
- Allow loopback Access
- Allow DNS
- Allow rsync from outside
- Allow postfix or sendmail
```

```
#!/bin/bash
iptables -F
iptables -P INPUT DROP
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
iptables -I INPUT –i eth0 –p tcp –s $VAGRANT_IP –d $SERVER_IP –dport 22 –j ACCEPT
iptables –A INPUT –i eth0 –p tcp –dport 80 –j ACCEPT
iptables –A INPUT –i eth0 –p tcp –dport 443 –j ACCEPT
iptables –A INPUT –i eth0 –p icmp-type 8 –s 0/0 –d $SERVER_IP –m state –state NEW,ESTABLISHED,RELATED –j ACCEPT
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT
iptables -A INPUT -i eth0 -p udp --sport 53 -m state --state ESTABLISHED -j ACCEPT
iptables -A OUTPUT -o eth0 -p udp --dport 53 -m state --state NEW,ESTABLISHED -j ACCEPT
sudo iptables -A INPUT -p tcp -s 0/0 --dport 873 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo iptables -A OUTPUT -p tcp --sport 873 -m conntrack --ctstate ESTABLISHED -j ACCEPT
iptables -A INPUT -p tcp --dport 25 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp --sport 25 -m state --state ESTABLISHED -j ACCEPT
```
